/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.hol.obisidiax.uf4.herencia.portbarcelona;

/**
 *
 * @author Danel
 */
public class Contenidor {
    
    static final int MAX_MERCADERIA=100;
    
    private String numSerie;
    private int capacitat;
    private int capacitat_restant;
    private  boolean estat;
    private Mercaderia albara[];
    private int tipus;

    public Contenidor(String numSerie, int capacitat, boolean estat, Mercaderia[] albara) {
        setNumSerie(numSerie);
        setCapacitat(capacitat);
        setCapacitat_restant(capacitat);
        setEstat(estat);
        setAlbara(albara);
    }

    public Contenidor(String numSerie, int capacitat, boolean estat) {
        setNumSerie(numSerie);
        setEstat(estat);
        setCapacitat(capacitat);
        setCapacitat_restant(capacitat);
    }

    public Contenidor(String numSerie, int capacitat) {
        setNumSerie(numSerie);
        setCapacitat(capacitat);
        setCapacitat_restant(capacitat);
    }

    public Contenidor(String numSerie) {
        setNumSerie(numSerie);
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        if (numSerie==null) {
            throw new IllegalArgumentException();
        } else {
            this.numSerie = numSerie;
        }
    }

    public int getCapacitat() {
        return capacitat;
    }

    public void setCapacitat(int capacitat) {
        if (this.albara==null) {
            throw new IllegalArgumentException();
        } else {
            this.capacitat = capacitat;
            this.capacitat_restant=capacitat;
        }
    }

    public boolean isEstat() {
        return estat;
    }

    public void setEstat(boolean estat) {
        if (estat || !estat) {
            this.estat = estat;
        } else {
            throw new IllegalArgumentException();
        }
        
    }

    public Mercaderia[] getAlbara() {
        return albara;
    }

    public void setAlbara(Mercaderia[] albara) {
        if (this.estat) {
            this.albara = albara;
        } else {
            throw new IllegalArgumentException();
        }
    }
    public void setTipus(int tipus) {
        this.tipus=tipus;
    }
    public void setCapacitat_restant(int capacitat) {
        this.capacitat_restant=capacitat;
    } 
    public void afegirMercaderies(Mercaderia[] mercaderia) {
        int volum=0;
        int quant;
        if (this.estat && this.tipus!=2) {
            for (Mercaderia mercaderia1 : mercaderia) {
                if (this.tipus==1) {
                    volum = volum + mercaderia1.getVolum();
                } else {
                    volum = (int) (volum + mercaderia1.getVolum()+(mercaderia1.getVolum()*0.01));
                }
                
            }
            if (volum < this.capacitat_restant ) {
                quant=this.getAlbara().length+mercaderia.length;
                capacitat_restant=this.capacitat_restant-volum;
                if (quant<=100 || capacitat_restant>0) {
                    setAlbara(mercaderia);
                    setCapacitat_restant(capacitat_restant);
                    if(capacitat_restant==0 || quant==100) {
                        setEstat(false);
                    }
                }
            }
        } else if(this.estat && mercaderia.length==1) {
            volum=mercaderia[0].getVolum()/1000;
            capacitat_restant=this.capacitat_restant-volum;
            setEstat(false);
        }
    }
}
