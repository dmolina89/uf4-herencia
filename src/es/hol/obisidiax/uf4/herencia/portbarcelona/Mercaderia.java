/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.hol.obisidiax.uf4.herencia.portbarcelona;

/**
 *
 * @author Danel
 */
public class Mercaderia {
    
    private String descripcio;
    private int volum;

    public Mercaderia(String descripcio, int volum) {
        this.descripcio = descripcio;
        this.volum = volum;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        if (descripcio.equals("")) {
            throw new IllegalArgumentException();
        }
            this.descripcio = descripcio;
    }

    public int getVolum() {
        return volum;
    }

    public void setVolum(int volum) {
        if (volum<0){
            throw new IllegalArgumentException();
        } else {
            this.volum = volum;
        }
    }
}
