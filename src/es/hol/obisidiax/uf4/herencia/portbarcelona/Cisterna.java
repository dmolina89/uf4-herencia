/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.hol.obisidiax.uf4.herencia.portbarcelona;

/**
 *
 * @author Danel
 */
public class Cisterna extends Contenidor {
    
    public Cisterna(String numSerie, int capacitat, boolean estat, Mercaderia[] albara) {
        super(numSerie, capacitat, estat, albara);
        super.setTipus(2);
    }
    public Cisterna(String numSerie, int capacitat, boolean estat) {
        super(numSerie, capacitat, estat);
        super.setTipus(2);
    }
    public Cisterna(String numSerie, int capacitat) {
        super(numSerie, capacitat);
        super.setTipus(2);
    }
    public Cisterna(String numSerie) {
        super(numSerie);
        super.setTipus(2);
    }

    public int litres() {
        return super.getCapacitat()*1000;
    }
    
}
