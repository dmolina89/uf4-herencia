/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.hol.obisidiax.uf4.herencia.portbarcelona;

/**
 *
 * @author Danel
 */
public class Refrigerat extends Contenidor {
    
    private int temperatura;
    
    public Refrigerat(String numSerie, int capacitat, boolean estat, Mercaderia[] albara, int temperatura) {
        super(numSerie, capacitat, estat, albara);
        setTemperatura(temperatura);
        super.setTipus(3);
    }
    public Refrigerat(String numSerie, int capacitat, boolean estat, Mercaderia[] albara) {
        super(numSerie, capacitat, estat, albara);
        super.setTipus(3);
    }
    public Refrigerat(String numSerie, int capacitat, boolean estat) {
        super(numSerie, capacitat, estat);
        super.setTipus(3);
    }
    public Refrigerat(String numSerie, int capacitat) {
        super(numSerie, capacitat);
        super.setTipus(3);
    }
    public Refrigerat(String numSerie) {
        super(numSerie);
        super.setTipus(3);
    }

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }
    
}
