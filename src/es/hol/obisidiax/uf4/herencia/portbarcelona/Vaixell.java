/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.hol.obisidiax.uf4.herencia.portbarcelona;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author Danel
 */
public class Vaixell {
    final static int MAX_CONTENIDOR=1000;
    
    Contenidor bodega[];

    public Vaixell(Contenidor[] bodega) {
        setBodega(bodega);
    }

    public Vaixell() {
    }

    public Contenidor[] getBodega() {
        return bodega;
    }

    public void setBodega(Contenidor[] bodega) {
        if (bodega.length>MAX_CONTENIDOR) {
            throw new IllegalArgumentException();
        } else {
            this.bodega=bodega;
        }
    }
    public void carregarContenidor(Contenidor contenidor) {
        if (this.bodega.length>=MAX_CONTENIDOR && !contenidor.isEstat()) {
            this.bodega[this.bodega.length]=contenidor;
        }
    }
    public void descarregar() {
        System.out.println("PORT DE BARCELONA");
        Date today = new Date();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yy");
        String date = DATE_FORMAT.format(today);
        System.out.println("DATA: " + date);
        System.out.println("CONTENIDOR                           VOLUM");
        System.out.println("-------------------------------------------");
        int volum_total=0;
        for (Contenidor bodega1 : this.bodega) {
            String name=bodega1.getNumSerie();
            int numero=bodega1.getCapacitat();
            volum_total +=numero;
            System.out.printf("%S                        %d m3\n",name, numero);
        }
        System.out.printf("VOLUM TOTAL:            %d",volum_total);
        this.bodega=null;
    }
}
