/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.hol.obisidiax.uf4.herencia.portbarcelona;

/**
 *
 * @author Danel
 */
public class TestContenidors {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    }
    public void afegirMercaderies(Contenidor contenidor, Mercaderia[] lot) {
        contenidor.afegirMercaderies(lot);
    }
    public void carregarContenidor(Vaixell vaixell, Contenidor contenidor) {
        vaixell.carregarContenidor(contenidor);
    }
    public void descarregarVaixell(Vaixell vaixell) {
        vaixell.descarregar();
    }
    public void mostrarContenidors(Vaixell vaixell) {
        Contenidor albara[];
        albara = vaixell.getBodega();
        for (Contenidor albara1 : albara) {
            System.out.printf("%S_________%d mercaderia/es", albara1.getNumSerie(), albara1.getAlbara().length);
        }
    }
}
