/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.hol.obisidiax.uf4.herencia.portbarcelona;

/**
 *
 * @author Danel
 */
public class DryVan extends Contenidor {
    
    private String color;

    public DryVan(String numSerie, int capacitat, boolean estat, Mercaderia[] albara, String color) {
        super(numSerie, capacitat, estat, albara);
        super.setTipus(1);
        setColor(color);
    }
    public DryVan(String numSerie, int capacitat, boolean estat, Mercaderia[] albara) {
        super(numSerie, capacitat, estat, albara);
        super.setTipus(1);
    }
    public DryVan(String numSerie, int capacitat, boolean estat) {
        super(numSerie, capacitat, estat);
        super.setTipus(1);
    }
    public DryVan(String numSerie, int capacitat) {
        super(numSerie, capacitat);
        super.setTipus(1);
    }
    public DryVan(String numSerie) {
        super(numSerie);
        super.setTipus(1);
    }

    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
}
